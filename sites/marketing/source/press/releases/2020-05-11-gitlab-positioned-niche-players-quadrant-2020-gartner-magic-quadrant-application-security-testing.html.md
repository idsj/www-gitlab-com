---
layout: markdown_page
title: "GitLab Positioned in the Niche Players Quadrant of the 2020 Gartner Magic Quadrant for Application Security Testing"
description: "GitLab’s AST offering allows organizations to find vulnerabilities earlier in the development lifecycle."
twitter_image: "/images/opengraph/Press-Releases/press-release-gartner-mq-ast.png"
twitter_creator: "@gitlab"
twitter_site: "@gitlab"
twitter_image_alt: "Image with celebration emoji around GitLab's press release."
---

_Evaluation based on completeness of vision and ability to execute as an application security testing provider_

SAN FRANCISCO, CALIFORNIA — May 11, 2020 - [GitLab](https://about.gitlab.com/), the single application for the DevOps lifecycle, today announced it has been positioned by Gartner in the Niche Players quadrant of the [2020 Magic Quadrant report for Application Security Testing](https://about.gitlab.com/resources/report-gartner-mq-ast/) (AST). GitLab was one of eleven vendors evaluated in the report. This is GitLab’s first year placed within the Magic Quadrant report for AST. 

“We're thrilled to be included in the Gartner Magic Quadrant for Application Security Testing this year," said David DeSanto, director of product for Secure and Defend at GitLab. "To us, our developer-first approach has allowed us to be a leader in DevOps and now has put us in a unique position among our fellow security-focused vendors. We believe our placement as a Niche Player affirms the importance of security within continuous integration and the value of shifting security left. We think it also validates our place as a trusted advisor within the security market."

Gartner defines the [AST market](https://about.gitlab.com/resources/report-gartner-mq-ast/) as, “the buyers and sellers of products and services designed to analyze and test applications for security vulnerabilities.” The analyst firm recognizes that the AST market is evolving, “Initial examination of updated vendor results suggests the market is growing at a faster pace than originally projected.” Additionally, developers are a growing population of AST buyers, as organizations have begun to successfully adapt to agile and DevOps development processes. 

[GitLab’s AST offering](https://about.gitlab.com/resources/report-gartner-mq-ast/) includes static application security testing (SAST), secret detection, dynamic application security testing (DAST), dependency scanning, container scanning, and license compliance security scanners, all baked into GitLab’s continuous integration/continuous delivery (CI/CD) platform within GitLab’s Ultimate/Gold tier offering. This allows organizations to find vulnerabilities earlier in the development lifecycle and enables developer and security teams to work side by side during any point of the testing lifecycle, from the time code is committed and throughout modifications, approvals, and exceptions. Teams using GitLab Ultimate/Gold tier are empowered by GitLab’s vulnerability management functionality which includes Security Dashboards at the project, group, and instance level.

Evaluation criteria for ability to execute includes product or service, overall viability, sales execution/pricing, market responsiveness/record, marketing execution, customer experience and operations. Evaluation criteria for completeness of vision includes market understanding, marketing strategy, sales strategy, offering (product) strategy, business model, vertical/industry strategy, innovation and geographic strategy.

To read a complimentary copy of the full report, please visit [here](https://about.gitlab.com/resources/report-gartner-mq-ast/).

*Source: “Gartner, Magic Quadrant for Application Security Testing, Mark Horvath, Dionisio Zumerle, Dale Gardner, 29 April 2020.*

#### Gartner Disclaimer
Gartner does not endorse any vendor, product or service depicted in our research publications, and does not advise technology users to select only those vendors with the highest ratings or other designation. Gartner research publications consist of the opinions of Gartner's research organization and should not be construed as statements of fact. Gartner disclaims all warranties, expressed or implied, with respect to this research, including any warranties of merchantability or fitness for a particular purpose.

#### About GitLab
GitLab is a DevOps platform built from the ground up as a single application for all stages of the DevOps lifecycle enabling Product, Development, QA, Security, and Operations teams to work concurrently on the same project. GitLab provides a single data store, one user interface, and one permission model across the DevOps lifecycle. This allows teams to significantly reduce cycle time through more efficient collaboration and enhanced focus. Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprises, including Ticketmaster, Jaguar Land Rover, NASDAQ, Dish Network, and Comcast trust GitLab to deliver great software faster. GitLab is the world's largest all-remote company, with more than 1,200 team members in more than 65 countries and regions.

#### Media Contact
Christina Weaver
<br>
GitLab
<br>
press@gitlab.com

