---
layout: handbook-page-toc
title: "Enterprise Applications Integrations"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Integrations Engineering

{::options parse_block_html="true" /}

<div class="panel panel-info">
**How to engage us**
{: .panel-heading}
<div class="panel-body">
Create an issue in our [issue tracker](https://gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/issue-tracker/-/issues/new) with the tag `BTG Integrations`. _Issue templates coming soon._
</div>
</div>

### Who we are

**Daniel Parker - Senior Integrations Engineer**
GitLab handle: [@djparker](https://gitlab.com/djparker)
Slack handle: @dparker
Job Family: [Integrations Engineer](/job-families/finance/integrations-engineer/#senior-integrations-engineer)

### Our mission

- DRI for the architecture, infrastructure, monitoring, auditing, and security of the enterprise applications ecosystem.  This includes data integrating between systems and coming in and out of the systems.
- Working closely with business partners to stabilize, improve and automate their business processes driving efficiencies and supporting seamless end to end processes.
- Achieve SOX and other compliance objectives in our enterprise application eco systems by establishing and maintaining standards for integration infrastructure, access controls, change management, security, monitoring and auditing.

### Our priorities

1. Drive improvement, stability and monitoring projects in both existing and new Zuora ingress and egress integrations.
    1. Ingress
        - SFDC (Salesforce.com)
        - GitLab customer portal (stable counterparts for Fulfillment engineering teams)
    1. Egress
        - SFDC (Salesforce.com)
        - GitLab customer portal (stable counterparts for Fulfillment engineering teams)
        - Netsuite
1. Perform a status quo health assessment of the enterprise applications ecosystem integrations.
1. DRI for Workato driven workflows to automate largely manual processes within Finance Systems.
1. Enable business partners to realise their efficiency goals by providing technical consultation on projects.
1. Drive and facilitate adoption of Workato for ad-hoc automations within G&A.

### Results we've delivered

We keep track of the projects we've delivered and the resulting efficiency improvements in _Work-hours per month_.

[See our results](results)
