---
layout: handbook-page-toc
title: Parental Leave and Return Tool Kit for Managers and Team Members
description: Parental Leave and Return Tool Kit for Managers and Team Members
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Manager Tool Kit

Do you have a team member preparing for parental leave and want to ensure you understand how to support? There are many things to consider when we think about parental leave as a manager. This toolkit is for everyone who is managing someone who is expecting to become, or has recently become, a parent.

### To-Do List

* Review our [parental leave benefits/policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave)      
* **Congratulate your team member** This is a time when many parents report feeling vulnerable, so your support will make all the difference.  Suggested things to say when you are told the news: Congratulations, How are you feeling?, How can we support you?, Do you know how to access the relevant maternity/shared parental/adoption policy?, Is the pregnancy/adoption confidential for now?
* **Virtual Baby (or Growing Your Family) Shower** Although we are fully distributed, there are many ways we can celebrate with our team members as people who work in office.  One of those ways could be a [virtual baby shower](#virtual-baby-or-growing-your-family-shower).
* **Arrange Out of Office Coverage** This may vary for work functions (i.e sales, etc). Consider what happens if the person needs to leave earlier than planned due to medical issues.
* **Manager todos while the team member is out of office:**:
   * Keep a running list in your 1:1 Agenda of significant changes or events while they were out on leave to fill them in on once they are back. For example, it may helpful to link to the specific Group Conversions that pertain to their role for ease in finding these resources upon return.
* **Ask how the team member wants to be communicated to while out (if at all):** Ask if the person would like you to check in during leave. Some team members might want to chat informally, to ensure they are not forgotten or feel as though they have an impact on the company. Ensure any communication while the team member is on leave is _not_ work related.
* **Communication with the team member as the transition back to work:** For a parent coming back to work there can be high levels of uncertainty on what to expect when they return. A couple examples to help the transition back to work can be:
   * First, ask how they are doing.
   * Update the team member on what they have missed since they have been out and any context you have captured in the 1:1 doc.
   * If possible, advise the team member to archive all emails when they come back to start fresh. If there is something they need to follow up on, have their colleagues follow up in a new correspondence or add them to an active thread.
   * Recommend that they read [GitLab's guide to parenting as a remote worker](/company/culture/all-remote/parenting/)

### General Guidelines For Managers on Resource Allocation 

If you have a team member going out on parental leave, a common question might be "how should I reallocate their tasks and output while they are out?" Below are general guidelines for managers based on the length of leave taken by the team member. Note - local law supersedes these guidelines where applicable:

* Leave less than 1 month: Use internal resources. 
* Leave less than 6 months: Use internal resources who may be eligible for an [interim bonus](/handbook/total-rewards/compensation/#compensation-for-interim-roles), and/or consider hiring a temporary contractor
* Leave longer than 6 months: Use internal resources who may be eligible for an [interim bonus](/handbook/total-rewards/compensation/#compensation-for-interim-roles), hire a temporary contractor. 
* Leave longer than 1 year: Consider a backfill ensuring there will be a similar (level, scope, etc) role for the person when they return.

## Parental Leave Reentry Buddies

Fellow GitLab team members, regardless of whether they are a parent, are encouraged to volunteer as a Parental Leave Reentry Buddy.

At a high level, a Parental Leave Reentry Buddy volunteers to help team members coming back online after [parental leave](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave). It is recommended that this buddy work within the same department as the team member reentering work, but this is not a requirement. If you are asked to be a buddy, work with your manager to delegate or temporarily pause certain duties to assist a team member as they ramp back up to 100%.

As a guide, Parental Leave Reentry Buddies should set aside around 10% to 20% of their time to assist with reentry for two to four weeks.

Responsibilities include:

1. Facilitate [coffee chats](/company/culture/all-remote/informal-communication/#coffee-chats) to catch up with team members on non-work topics.
1. Answer questions about top concerns, projects, and milestones that occur during parental leave.
1. Surface relevant handbook pages that are created during parental leave.
1. Advise of relevant new hires onboarded during parental leave.

*Specifics on responsibilities and expectations are still being considered, and will be added to this section in future iterations.*

### Find a Parental Leave Reentry Buddy

Visit the [GitLab Team page](/company/team/) and search for 'Parental Leave Reentry Buddy', or ask on Slack in `#intheparenthood`.

### Become a Parental Leave Reentry Buddy

If you're comfortable using Git and GitLab and want to help team members troubleshoot problems and accelerate their learning, please follow these steps to indicate your availability as a `Parental Leave Reentry buddy`:

1. Find your entry in the [team.yml](/handbook/git-page-update/#11-add-yourself-to-the-team-page) file.
1. Add `Parental Leave Reentry buddy` to the `departments` section in your entry (keeping your existing departments):
   ```
   departments:
     - ...
     - Parental Leave Reentry buddy
   ```
1. Add the following code above the `story` section in your entry:
   ```
     expertise:  |
                 <li><a href="/handbook/people-group/general-onboarding/parental-leave-buddy/">Parental Leave Reentry buddy</a></li>
   ```
1. If you already have an `expertise` section, add the list item portion of the above code:
   ```
                 <li><a href="/handbook/people-group/general-onboarding/parental-leave-buddy/">Parental Leave Reentry buddy</a></li>
   ```

## Virtual Baby (or Growing Your Family) Shower

A nice way to celebrate a team member's new addition to their family is by hosting a virtual baby/growing your family shower. In colocated companies, this is something that teams often do to wish a team member well as they head out on parental or adoption leave. We encourage virtual showers to be considered for all team members (not just expectant moms). In order to help teams host a virtual shower, here are some resources that can help you get started:
1. Ask the team member if they would be comfortable with a virtual shower. In a remote environment, surprising the parent-to-be is less effective as they need to be an active participant in the process.
1. If the team member is comfortable and open to an event, ask them who they would like to include in the celebration and if they have any gift registries that other team members can buy gifts from so they are purchasing items that the baby will need. The purchase of gifts should always be optional.
1. Send out an invite in advance of the event so attendees can plan to be available for that date and time.
1. Create an agenda for the event. Be mindful to create an inclusive and welcoming atmosphere. Consider running potential games by the celebrated team member to ensure comfortability. See a sample agenda below:
* Kick off the meeting by thanking everyone for coming, review the agenda and how games and prizes will be work.  
* Start meeting off with opening ice breaker: An option is a Baby MadLibs game.
* Game 1 - Measure the belly. If the soon-to-be parent is an expecting mom, you can play this game. It requires a roll of toilet paper. Participants guess how many sheets of toilet paper it will take to wrap around the expecting mom's belly. The closest guess gets a prize.
* Open presents - Have the soon-to-be parent save the presents shipped to them to be opened during the celebration.
* Game 2 - Songs with the word "Baby." Have participants write as many songs as they can think of that have the word "baby" in them. Whoever thinks of the most songs within three minutes, wins a prize.  
* Close the event with an open forum for people to ask fun questions and send positive affirmations.

## Team Member Tool Kit

* Review our [parental leave benefits/policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave)
* Slack channel to connect with other parents — `#intheparenthood` ([Join Slack channel](https://gitlab.slack.com/app_redirect?channel=intheparenthood) - *for GitLab team members only*)
* Read the handbook section on [returning from parental leave](/handbook/paid-time-off/#returning-to-work-after-parental-leave) with tips for the transition back to work
* Read [GitLab's guide to parenting as a remote worker](/company/culture/all-remote/parenting/)
* Consider reaching out to a [Parental Leave Reentry Buddy](#parental-leave-reentry-buddies)

## Parental Leave Policy Feedback

If you have any feedback about your parental leave experience for the People group to review you can always email `total-rewards@gitlab.com` or open an issue using our [Parental Feedback Issue Template](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

----

Return to the main [GitLab Benefits page](/handbook/total-rewards/benefits/).
