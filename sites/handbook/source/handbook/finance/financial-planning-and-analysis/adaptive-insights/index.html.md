---
layout: handbook-page-toc
title: "Adaptive Insights"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## General Information

[Adaptive Insights](https://www.adaptiveplanning.com/), a Workday company, is a cloud-based corporate performance management platform. The GitLab FP&A team utilizes Adaptive Inisghts to plan, budget, and forecast GitLab's planning cycles. 

## Administration

#### Versioning 

One of the most powerful features of Adaptive Insights is versioning. Adminstrators can create, edit, and lock versions of a plan. 

##### GitLab Version Nomenclature

Each version of GitLab should specify the fiscal year and label of the plan / forecast. Below is a few examples of the nomenclature used at GitLab

- `FY21 - 0 + 12 Plan`
- `FY21 - 1 + 11 Forecast`
- `FY 21 - 2 + 10 Forecast`

#### Roles

There are currently five different roles within GitLab's Adaptive Insights instance

1. Administrative - full control of Adaptive Insights
1. Analysis - can access sheets, reports, dashboards, and process trackers. Analysis can also share reports and complete tasks using the process tracker. 
1. IT Administrator - full control of Adaptive Insights with the exception to salary details
1. Report Only - same as Analysis but can upload files for reporting
1. Standard - full control of Adaptive Insights with the exception to edit, modify or change structure. Standard also does not have admin or integration access

#### Users

Within Adpative, the administrator can assign a name, position, user name, home page, time zone, country, role, and level(s) for each user. The adminstrator can also reset the password, if needed, for the User. Users should use Okta to sign into Adaptive Insights but keep their password in One Password when assigned one. 

If you need access to Adaptive Insights, please open an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests) and assign to the code owner of this page. 

#### Accounts

#### Assumptions

#### Currencies

#### Allocation Rules

## Sheets

### Level Assigned Sheets

#### Balance Sheet

#### Income Statement

#### Cash Flow

#### Active Personnel 

#### Planned Personnel

#### Program Spend

#### Travel & Entertainment

### User Assigned Sheets

#### Global Assumptions

#### SUI Assumptions by State

#### Allocations

#### Salary Range

#### Personnel Benefits by Level

#### Balance Sheet Assumptions

#### Commissions %

#### Revenue Model 


## Integrations

#### NetSuite

#### Adpative Loaders

#### Scheduling

## Formulas


## Reports

#### Report Structure 

#### Reports 





