---
layout: markdown_page
title: "FY21 Architecture Roadmaps & Blueprints"
---

## FY21 Architecture Roadmaps & Blueprints


### FY21 Roadmaps

1. [GitLab.com Migration to Cloud Native](https://about.gitlab.com/handbook/engineering/infrastructure/production/kubernetes/gitlab-com/)
   * [Blueprints](../blueprints/cloud_native_gitlab)

#### FY21-Q4 Blueprints