- name: Enablement Section - Section PPI - Median End User Latency
  base_path: "/handbook/product/performance-indicators/"
  definition: Median end user latency collected by Real User Monitoring. This metric
    captures how well we are doing in serving our primary customers, our end users,
    by measuring their end user experience.
  target: TBD
  org: Enablement Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined, as metric is not instrumented yet
  instrumentation:
    level: 1
    reasons:
    - Waiting on [RUM to be implemented](https://gitlab.com/gitlab-org/gitlab/-/issues/218507)
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5657

- name: Enablement:Distribution - PPI - Percentage of installations on the 3 most
    recent versions of GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: Of the total number of self-managed installs, what percentage are on
    one of the three most recent versions.
  target: 40%
  org: Enablement Section
  public: true
  telemetry_type: 
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - A target of 40% has been set. 40% is the upgrade rate that was reached in early 2019.
    - Insights - The upgrade rate continued to fall to 26% in August. The decline in upgrade rate is evident across all major install methods. August data is added on September 1. At that point the three latest releases were 13.1, 13.2, and 13.3. The steady decline in upgrade rate started in May. The May data counts 13.0 as the latest release. For Omnibus installs, we are now at the lowest upgrade rate observed in 18 months.  
  instrumentation:
    level: 2
    reasons:
    - Instrumentation of some sub metrics is still in progress
  sisense_data:
    chart: 8890933
    dashboard: 406972
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4789
  - https://gitlab.com/gitlab-data/analytics/-/issues/4791
  - https://gitlab.com/gitlab-data/analytics/-/issues/4800
  - https://gitlab.com/gitlab-data/analytics/-/issues/4801
  - https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=5697571&udv=832292

- name: Enablement:Geo - Paid GMAU - Number of unique users utilizing a Geo secondary
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of unique users utilizing a Geo secondary. This adoption metric
    helps us understand whether end users are actually seeing value in, and are using,
    geo secondaries.
  target: TBD
  org: Enablement Section
  public: true
  telemetry_type: 
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet, as we don't have instrumentation
    - Insight -  The number of [Geo nodes deployed](https://app.periscopedata.com/app/gitlab/500159/Geo?widget=6472090&udv=0)
      has stabilized over the second half of 2020. We know that Geo has low
      penetration as a percentage of total deployments, but skews heavily toward the
      large enterprise with a [25% percentage of Premium+ user share](https://docs.google.com/presentation/d/1imw_PWKZhJpxRs_VwTa-SWgwbZJhw2jKp6wMRk2Fo3c/edit#slide=id.g807f195cca_0_768).
    - Improvement - We are working to add support for replicating all data types,
      so that Geo is on a solid foundation for both DR and Geo Replication.
  instrumentation:
    level: 1
    reasons:
    - Geo is not available on GitLab.com today, so cannot use Snowplow or the .com
      database.
    - Geo secondaries [do not support usage ping today](https://gitlab.com/gitlab-org/gitlab/-/issues/231257) but work has started
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/231257

- name: Enablement:Memory - PPI - Requests / Compute
  base_path: "/handbook/product/performance-indicators/"
  definition: Requests per Hour (Rolling 7 day average) / Compute Cost. This metric
    is a measure of efficiency, measuring how many requests GitLab can service for
    one compute dollar. As GitLab is more efficient with CPU and Memory, it will go
    up.
  target: TBD
  org: Enablement Section
  public: true
  telemetry_type: 
  is_primary: true
  is_key: false
  sisense_data:
    chart: 9627495
    dashboard: 679200
    embed: v2
  health:
    level: 0
    reasons:
    - No target defined yet, variance in current NSM is too high. Discussing [how to resolve](https://gitlab.com/gitlab-data/analytics/-/issues/4435#note_406471543).
    - Insight - This metric, as defined, focuses largely on backend efficiency. While
      this is broadly a good measure of the work Memory does, we are currently focused
      on an improvement which will actually increase backend compute for a significant
      decrease in end user latency. We believe this is a one-off and therefore not
      worth adjusting the metric, but if this becomes a pattern we can re-evaluate.
    - Insight - GitLab.com is [too slow](https://forgeperf.org).
    - Improvement - We are working to [resize images](https://gitlab.com/groups/gitlab-org/-/epics/3979)
      which will significantly improvement latency, in addition to other smaller changes.
  instrumentation:
    level: 1
    reasons:
    - Switching from total Memory/CPU to [memory/cpu utilization](https://gitlab.com/gitlab-org/gitlab/-/issues/230898),
      to reduce the impact of unutilized instances.
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/230898

- name: Enablement:Global Search - GMAU - Number of unique users interacting with
    Global Search per month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique active users interacting with either Basic Search or Advanced Search per Month.
  target: TBD
  org: Enablement Section
  public: true
  telemetry_type: 
  is_primary: 
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet
  instrumentation:
    level: 1
    reasons:
    - Global Search GMAU Implementation is in progress using a [Redis HLL](https://gitlab.com/gitlab-org/gitlab/-/issues/232831). Available in GitLab 13.4, releasing on October 2020. 
  sisense_data:
    chart: 9087857
    dashboard: 596072
    embed: v2
- name: Enablement:Advanced Search - Paid GMAU - The number of unique paid
    users per month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique paid users interacting with Advanced Search
    per month.
  target: TBD
  org: Enablement Section
  public: true
  telemetry_type: 
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet
  instrumentation:
    level: 1
    reasons:
    - Global Search GMAU  Implementation is in progress using a [Redis HLL](https://gitlab.com/gitlab-org/gitlab/-/issues/232831). I should be available in October 2020. 
- name: Enablement:Database - PPI - Database Query Apdex (100ms target, 250ms tolerable)
  base_path: "/handbook/product/performance-indicators/"
  definition: Database query Apdex, with 100ms the target and 250ms tolerable. This
    measures the ratio of queries which complete within the satisfactory time, informing
    how well the database is scaling and performing.
  target: TBD
  org: Enablement Section
  public: true
  telemetry_type: 
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet, pending metric results
    - Improvement - We are focusing on [partitioning](https://gitlab.com/groups/gitlab-org/-/epics/2023)
      the largest tables to improveme the performance and scalability of the database.
  instrumentation:
    level: 1
    reasons:
    - With Prometheus querying now supported, adding [database instrumentation](https://gitlab.com/gitlab-org/gitlab/-/issues/227305)
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/227305

- name: Enablement:Infrastructure - GMAU - Number of unique users that perform an
    action on GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users that performed an action on gitlab.com in
    a 28 day rolling period.
  target: TBD
  org: Enablement Section
  public: true
  telemetry_type: 
  is_primary: 
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet
  instrumentation:
    level: 2
    reasons:
    - Next step, add breakdown by tier
  sisense_data:
    chart: 8079819
    dashboard: 527913
    embed: v2

- name: Enablement:Infrastructure - Paid GMAU - Number of unique paid users that perform
    an action on GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all monthly active users on gitlab.com that roll up to paid
    accounts in a 28 day rolling period.
  target: TBD
  org: Enablement Section
  public: true
  telemetry_type: 
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet
  instrumentation:
    level: 1
    reasons:
    - Need to verify [dashboard](https://app.periscopedata.com/app/gitlab/710777/Infra-PM-Dashboard)
      is accurate
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5434
