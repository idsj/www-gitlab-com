pages:
  - path: bdm
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      header_link: /resources/gitlab-vs-atlassian/
      page_title: Bitbucket vs. Gitlab for Business Decision Makers
      css: devops-tools/extra-content.css
      page_body: |
        ## Summary

        Atlassian Bitbucket gives teams Git code management, but also one place to plan projects, collaborate on code, test and deploy. It is marketed in the SaaS form (Bitbucket Cloud) and in a self-managed version (Bitbucket Server), however they are not the same product. Bitbucket Server is simply a [re-branding of Stash](https://www.atlassian.com/blog/archives/atlassian-stash-enterprise-git-repository-management). The two products are completely different code bases, written in two different languages ([Cloud in Python, Server in Java](https://en.wikipedia.org/wiki/Bitbucket)) and do not maintain feature parity. Because of separate codebases they each have a completely different API, making it much harder to integrate.

        Bitbucket supports  Mercurial or Git, but not SVN. GitLab does not support Mercurial or SVN.
        GitLab is a complete DevOps platform, delivered as a single application, with built-in project management, source code management, CI/CD, monitoring and more. Bitbucket only does source code management. You would need to use Atlassian Jira to get project management, Bamboo for CI/CD and Atlassian does not provide a monitoring solution. Additionally, GitLab Ultimate comes with robust built-in security capabilities such as SAST, DAST, Container Scanning, Dependency Scanning, License Compliance, secret detection and more. Bitbucket does not support these capabilities and Atlassian does not have a product for them.

        GitLab also offers an "on-prem" self-managed and "cloud" SaaS solution. GitLab runs the exact same code on its SaaS platform that it offers to its self-hosted customers. This means customers can migrate from self-hosted to SaaS and back relatively easily and each solution maintains feature parity.

        GitLab has seen an increased interest from [world-wide internet searches](https://trends.google.com/trends/explore?date=today%205-y&q=bitbucket,gitlab) (a strong indicator for devops interest) over the past five years.
        <script type="text/javascript" src="https://ssl.gstatic.com/trends_nrtr/1845_RC03/embed_loader.js"></script> <script type="text/javascript"> trends.embed.renderExploreWidget("TIMESERIES", {"comparisonItem":[{"keyword":"bitbucket","geo":"","time":"today 5-y"},{"keyword":"gitlab","geo":"","time":"today 5-y"}],"category":0,"property":""}, {"exploreQuery":"date=today%205-y&q=bitbucket,gitlab","guestPath":"https://trends.google.com:443/trends/embed/"}); </script>

        Between 2018 and 2019 Gitlab adoption as a Version Control System has increased by 21% whereas Atlassian Bitbucket's adoption has decreased by 11%.  This is as per [The Next Stack's analysis](https://pbs.twimg.com/media/EGoXqXeXUAE_Dva.png) of Jetbrains Developer Ecosystem surveys.

        ## Bitbucket Strengths

        ![GitLab Bitbucket Strengths](/devops-tools/bitbucket/images/BB-Strengths.png)

        ## Bitbucket Limitations and Challenges

        ![GitLab Bitbucket Strengths](/devops-tools/bitbucket/images/BB-Weaknesses.png)

        ## GitLab Differentiators

        ![GitLab Bitbucket Strengths](/devops-tools/bitbucket/images/GitLab-BB-Differentiators.png)

  - path: license
    content:
      page_title: GitLab vs. Bitbucket License Overview
      css: devops-tools/extra-content.css
      page_body: |
        ## Bitbucket vs. GitLab Pricing (Cloud)

          |                                                                   | <br>Free                    | <br>Bitbucket<br>Standard   | <br>Bitbucket<br>Premium    | GitLab<br>Bronze  |
          |-------------------------------------------------------------------|-----------------------------|-----------------------------|-----------------------------|-------------------|
          | <br>Cost                                                          | $0 <br>user/month           | $3 <br>user/month           | $6 <br>user/month           | $4 <br>user/month |
          | <br>Users                                                         | <br>5 or less               | <br>Unlimited               | <br>Unlimited               | <br>Unlimited     |
          | <br>Build minutes/month                                           | <br>50                      | <br>2500                    | <br>3500                    | <br>2000          |
          | <br>CI/CD                                                         | <br>Yes                     | <br>Yes                     | <br>Yes                     | <br>Yes           |
          | <br>Unlimited Private Repos                                       | <br>Yes                     | <br>Yes                     | <br>Yes                     | <br>Yes           |
          | Issue Management/<br>Team Collaboration                           | Jira and Trello Integration | Jira and Trello Integration | Jira and Trello Integration | Yes <br>Native    |
          | Code Insights <br>(Visibility into insights from 3rd party tools) | 3 <br>integrations          | <br>Unlimited               | <br>Unlimited               | <br>Unlimited     |
          | <br>Smart Mirroring                                               | No                          | No                          | Yes                         | Yes               |
          | <br>Deployment Permissions                                        | No                          | No                          | Yes                         | No                |
          | Merge Checks/<br>Enforced Merge Checks                            | <br>Yes/No                  | <br>Yes/No                  | <br>Yes/Yes                 | <br>Yes/Yes       |

          * GitLab Bronze/Starter offers
              * Native Issue Management
              * Native Team Collaboration Features
              * Remote repository mirroring
              * Built in Container Registry
              * Native Analytics
              * Forced Merge Approvals

        ## Bitbucket Pricing (Self-Managed)

          |                 | Server    | Data Center |
          |-----------------|-----------|-------------|
          | License Type    | Perpetual | Annual Term |
          | 25 user Cost    | $2,900    | $1,980      |
          | 50 user Cost    | $5,200    | $3,630      |
          | 100 user Cost   | $9,500    | $6,600      |
          | 250 user Cost   | $19,000   | $13,200     |
          | 500 user Cost   | $25,300   | $17,600     |
          | 1,000 user Cost | $35,000   | $26,400     |
          | 2,000 user Cost | $69,800   | $52,800     |

          * Perpetual license
            * Includes 1 year of maintenance
            * Additional maintenance cost must be considered after year 1
            * Includes 1 server only

          * Data Center
            * Includes Smart Mirroring
            * Includes Disaster Recovery
            * Gets very costly as user count reduces

  - path: key-features
    content:
      page_title: Bitbucket Key Features
      css: devops-tools/extra-content.css
      page_body: |
        ## Issue Tracking

        | Tight integration with Jira for issue management |                                                                                                                    |
        |:------------------------------------------------:|--------------------------------------------------------------------------------------------------------------------|
        |                                                  | Code repos can be linked to projects and issues                                                                    |
        |                                                  | Cross tool functions: Create/View/Edit Jira issue in Bitbucket and create Pull Request and Branches in Jira issues |
        |                                                  | Jira very popular and highly used for issue management                                                             |

        ## Collaboration

        | Tight Integration with Trello for team collaboration |                                                     |
        |:----------------------------------------------------:|-----------------------------------------------------|
        |                                                      | Code repos can be linked to task                    |
        |                                                      | Planning and Collaboration can be done in Bitbucket |

        ## Version Control

        | Free unlimited private repos |                                                 |
        |------------------------------|-------------------------------------------------|
        |                              | Up to 5 collaborators                           |
        |                              | Allows public access to repos                   |
        |                              | Tight integration with other Atlassian products |
        |                              | Pull Request and Code Reviews                   |
        |                              | Branch comparisons                              |
        |                              | Commit History                                  |
        |                              | SOC 2 Reporting                                 |

        ## Continuous Integration & Continuous Delivery

        | Bitbucket Pipelines  |                                                                                |
        |----------------------|--------------------------------------------------------------------------------|
        |                      | Built into Bitbucket                                                           |
        |                      | Bitbucket Cloud Only                                                           |
        |                      | Linux based agent (runner)                                                     |
        |                      | Language specific templates                                                    |
        |                      | Build configurations stored in Bitbucket yml file                              |
        |                      | Build status visibility in Jira                                                |
        |                      | Automatic unlimited concurrency - <br>run any number of pipelines concurrently |
        |                      | Pipeline requires a few clicks to activation/setup                             |

  - path: faq
    content:
      page_title: Questions You Should Ask and Answers to Concerns You May have
      css: devops-tools/extra-content.css
      page_body: |
        ## Questions To Ask When Considering Bitbucket

        ![GitLab Bitbucket Strengths](/devops-tools/bitbucket/images/BB-Questions.png)

        ## Aswers To Your Migration Concerns

        ![GitLab Bitbucket Strengths](/devops-tools/bitbucket/images/BB-Concerns.png)
